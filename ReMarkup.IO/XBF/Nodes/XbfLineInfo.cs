﻿namespace ReMarkup.IO.XBF.Nodes
{
    public record struct XbfLineInfo(uint LineNumber, uint LinePosition);
}
