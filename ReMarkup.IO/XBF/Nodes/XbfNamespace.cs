﻿namespace ReMarkup.IO.XBF.Nodes
{
    public class XbfNamespace : XbfNode
    {
        public string Namespace { get; }

        public XbfNamespace(BinaryReader br, XbfLineInfo lineInfo) : base(br, lineInfo)
        {
            uint length = br.ReadUInt32();

            Namespace = new string(br.ReadChars((int)length));
        }
    }
}
