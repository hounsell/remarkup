﻿namespace ReMarkup.IO.XBF.Nodes
{
    public class XbfNode
    {
        public uint Id { get; }
        public uint Flags { get; }

        public XbfLineInfo LineInfo { get; }

        public XbfNode(BinaryReader? br, XbfLineInfo lineInfo)
        {
            LineInfo = lineInfo;

            if (br == null)
            {
                // custom node format - as of today, currently only XbfValue
                Id = 0;
                Flags = 0;
            }
            else
            {
                // standard node
                Id = br.ReadUInt32();
                Flags = br.ReadUInt32();
            }
        }
    }
}
