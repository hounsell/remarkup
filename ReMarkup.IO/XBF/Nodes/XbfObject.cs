﻿using System.Collections.Immutable;

namespace ReMarkup.IO.XBF.Nodes
{
    public class XbfObject : XbfNode
    {
        private readonly List<XbfProperty> properties = new();

        public IReadOnlyList<XbfProperty> Properties => properties.ToImmutableArray();

        internal void AddProperty(XbfProperty property)
        {
            properties.Add(property);
        }

        public XbfObject(BinaryReader br, XbfLineInfo lineInfo) : base(br, lineInfo)
        {
        }
    }
}
