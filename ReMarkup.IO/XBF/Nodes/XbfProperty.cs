﻿using System.Collections.Immutable;

namespace ReMarkup.IO.XBF.Nodes
{
    public class XbfProperty : XbfNode
    {
        private readonly List<XbfNode> values = new();

        public IReadOnlyList<XbfNode> Values => values.ToImmutableArray();

        internal void AddValue(XbfNode value)
        {
            values.Add(value);
        }

        public XbfProperty(BinaryReader br, XbfLineInfo lineInfo) : base(br, lineInfo)
        {
        }
    }
}
