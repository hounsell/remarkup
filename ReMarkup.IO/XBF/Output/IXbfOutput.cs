﻿namespace ReMarkup.IO.XBF.Output
{
    public interface IXbfOutput<T>
    {
        T GetOutput(XbfFile file);
    }
}
