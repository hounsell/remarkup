﻿using ReMarkup.IO.XBF.Nodes;
using ReMarkup.IO.XBF.Types;

namespace ReMarkup.IO.XBF.Output
{
    public readonly record struct XbfTreeItem(string Display, XbfTreeItem?[] Children);

    public class TreeOutput : IXbfOutput<XbfTreeItem?>
    {
        public XbfFile? CurrentFile { get; private set; }

        public XbfTreeItem? GetOutput(XbfFile file)
        {
            CurrentFile = file;

            return DumpXbfObject(CurrentFile.RootNode);
        }

        private XbfTreeItem? DumpXbfObject(XbfObject xo)
        {
            if (CurrentFile is null)
            {
                return null;
            }

            var obj = CurrentFile.TypeTable.Values[(int)xo.Id];
            string disp = CurrentFile.StringTable.Values[(int)obj.StringId];

            return new XbfTreeItem(disp, (from prop in xo.Properties
                            select DumpXbfProperty(prop)).ToArray());
        }

        private XbfTreeItem? DumpXbfProperty(XbfProperty xp)
        {
            if (CurrentFile is null)
            {
                return null;
            }

            var property = CurrentFile.PropertyTable.Values[(int)xp.Id];
            string disp = property.Flags.HasFlag(PropertyFlags.IsMarkupDirective) ?
                            string.Format("x:{0}", CurrentFile.StringTable.Values[(int)property.StringId]) :
                            CurrentFile.StringTable.Values[(int)property.StringId];

            return new XbfTreeItem(disp, (from node in xp.Values
                            select DumpXbfNode(node)).ToArray());
        }

        private XbfTreeItem? DumpXbfText(XbfText xt)
        {
            if (CurrentFile is null)
            {
                return null;
            }

            string disp = CurrentFile.StringTable.Values[(int)xt.Id];
            return new XbfTreeItem(disp, Array.Empty<XbfTreeItem?>());
        }

        private XbfTreeItem DumpXbfValue(XbfValue xv)
        {
            return new XbfTreeItem(string.Format("{0}: {1}", xv.Type, xv.Value), Array.Empty<XbfTreeItem?>());
        }

        private XbfTreeItem? DumpXbfNode(XbfNode xn)
        {
            if (xn is XbfObject xo)
            {
                return DumpXbfObject(xo);
            }
            else if (xn is XbfValue xv)
            {
                return DumpXbfValue(xv);
            }
            else if (xn is XbfText xt)
            {
                return DumpXbfText(xt);
            }
            else
            {
                return null;
            }
        }
    }
}
