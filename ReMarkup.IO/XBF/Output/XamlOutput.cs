﻿using ReMarkup.IO.XBF.Nodes;
using ReMarkup.IO.XBF.Types;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace ReMarkup.IO.XBF.Output
{
    public class XamlOutput : IXbfOutput<string>
    {
        public string GetOutput(XbfFile file)
        {
            StringBuilder output = new();
            Dictionary<string, string> namespaces = new();

            using XmlWriter writer = XmlWriter.Create(output, new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = true,
                NamespaceHandling = NamespaceHandling.OmitDuplicates
            });

            foreach (var ns in file.Namespaces)
            {
                var xns = file.StringTable.Values[(int)file.XmlNamespaceTable.Values[(int)ns.Id]];
                namespaces.Add(xns, ns.Namespace);
            }

            var rootObj = file.TypeTable.Values[(int)file.RootNode.Id];

            var (rootName, rootNs) = GetXNameForObject(file.StringTable.Values[(int)rootObj.StringId],
                file.StringTable.Values[(int)file.TypeNamespaceTable.Values[(int)rootObj.NamespaceId].StringId]);

            writer.WriteStartElement(namespaces[rootNs], rootName, rootNs);
            //if (rootXns != null && rootXns.Name.LocalName != "xmlns")
            //{
            //    writer.WriteStartElement(rootXns.Name.LocalName, rootName, rootXns.Value);
            //}
            //else
            //{
            //    writer.WriteStartElement(rootName, rootNs);
            //}

            foreach (var ns in file.Namespaces)
            {
                var xns = file.StringTable.Values[(int)file.XmlNamespaceTable.Values[(int)ns.Id]];
                writer.WriteAttributeString("xmlns", ns.Namespace, null, xns);
            }

            foreach (var prop in file.RootNode.Properties)
            {
                if (prop is null)
                {
                    continue;
                }

                DumpXbfPropertyToXml(writer, file, namespaces, prop);
            }

            writer.WriteEndElement();
            writer.Flush();

            return output.ToString();
        }

        private void DumpXbfObjectToXml(XmlWriter writer, XbfFile file, Dictionary<string, string> namespaces, XbfObject? xo)
        {
            if (xo == null)
            {
                return;
            }

            var obj = file.TypeTable.Values[(int)xo.Id];
            var (name, ns) = GetXNameForObject(file.StringTable.Values[(int)obj.StringId],
                file.StringTable.Values[(int)file.TypeNamespaceTable.Values[(int)obj.NamespaceId].StringId]);


            writer.WriteStartElement(namespaces[ns], name, null);


            foreach (var prop in xo.Properties)
            {
                DumpXbfPropertyToXml(writer, file, namespaces, prop);
            }

            writer.WriteEndElement();
        }

        private void DumpXbfPropertyToXml(XmlWriter writer, XbfFile file, Dictionary<string, string> namespaces, XbfProperty xp)
        {
            var property = file.PropertyTable.Values[(int)xp.Id];
            var isMarkupProperty = property.Flags.HasFlag(PropertyFlags.IsMarkupDirective);
            var (name, ns) = GetXNameForObject(file.StringTable.Values[(int)property.StringId],
                isMarkupProperty ? "x" : file.StringTable.Values[(int)file.TypeNamespaceTable.Values[(int)file.TypeTable.Values[(int)property.TypeId].NamespaceId].StringId]);

            foreach (var node in xp.Values)
            {
                switch (node)
                {
                    case XbfObject xo:
                        // implicititems?
                        var objType = file.TypeTable.Values[(int)xo.Id];
                        string typeName = file.StringTable.Values[(int)objType.StringId];
                        if (typeName == "Binding" || typeName == "TemplateBinding" || typeName == "StaticResource" || typeName == "ThemeResource")
                        {
                            List<string> props = new();
                            foreach (var prop in xo.Properties)
                            {
                                var innerProp = file.PropertyTable.Values[(int)prop.Id];
                                string propName = file.StringTable.Values[(int)innerProp.StringId];
                                var value = prop.Values.FirstOrDefault();
                                switch (value)
                                {
                                    case XbfValue innerXv:
                                        props.Add($"{propName}={innerXv.Value}");
                                        break;
                                    case XbfText innerXt:
                                        props.Add($"{propName}={file.StringTable.Values[(int)innerXt.Id]}");
                                        break;
                                }
                            }

                            writer.WriteAttributeString(namespaces[ns], name, null, $"{{{typeName} {string.Join(", ", props)}}}");
                        }
                        else
                        {
                            DumpXbfObjectToXml(writer, file, namespaces, xo);
                        }
                        break;
                    case XbfText xt:
                        if (name == "implicitinitialization")
                        {
                            writer.WriteValue(file.StringTable.Values[(int)xt.Id]);
                        }
                        else
                        {
                            writer.WriteAttributeString(namespaces[ns], name, null, file.StringTable.Values[(int)xt.Id]);
                        }
                        break;
                    case XbfValue xv:
                        writer.WriteAttributeString(namespaces[ns], name, null, xv.Value);
                        break;
                }

            }
        }

        private static (string name, string ns) GetXNameForObject(string type, string typeNamespace)
        {
            // filter out any invalid characters
            string dispType = Regex.Replace(type, @"[^\p{L}\p{N}]+", "");

            if (typeNamespace.StartsWith("Windows.UI"))
            {
                return (dispType, "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            }
            else if (typeNamespace == "x" || typeNamespace == "Windows.Foundation")
            {
                return (dispType, "http://schemas.microsoft.com/winfx/2006/xaml");
            }
            else
            {
                return (dispType, string.Format("using:{0}", typeNamespace));
            }
        }
    }
}
