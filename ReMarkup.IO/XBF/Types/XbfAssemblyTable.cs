﻿namespace ReMarkup.IO.XBF.Types
{
    public class XbfAssemblyTable : XbfTable<AssemblyEntry>
    {
        public XbfAssemblyTable(BinaryReader reader, Version fileVersion) : base(reader, fileVersion)
        {
        }

        protected override AssemblyEntry ReadValue(BinaryReader reader, Version fileVersion)
        {
            var kind = (AssemblyEntryType)reader.ReadUInt32();
            var id = reader.ReadUInt32();
            return new AssemblyEntry(kind, id);
        }
    }

    public readonly record struct AssemblyEntry(AssemblyEntryType ProviderKind, uint StringId);

    public enum AssemblyEntryType
    {
        Unknown = 0,
        Native = 1,
        Managed = 2,
        System = 3,
        Parser = 4,
        Alternate = 5
    }
}
