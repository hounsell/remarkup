﻿namespace ReMarkup.IO.XBF.Types
{
    public class XbfPropertyTable : XbfTable<PropertyEntry>
    {
        public XbfPropertyTable(BinaryReader reader, Version fileVersion) : base(reader, fileVersion)
        {
        }

        protected override PropertyEntry ReadValue(BinaryReader reader, Version fileVersion)
        {
            var flags = (PropertyFlags)reader.ReadUInt32();
            var typeId = reader.ReadUInt32();
            var stringId = reader.ReadUInt32();
            return new PropertyEntry(flags, typeId, stringId);
        }
    }

    public readonly record struct PropertyEntry(PropertyFlags Flags, uint TypeId, uint StringId);

    [Flags]
    public enum PropertyFlags
    {
        None = 0,
        IsXmlProperty = 1,
        IsMarkupDirective = 2,
        IsImplicitProperty = 4
    }
}
