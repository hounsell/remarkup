﻿namespace ReMarkup.IO.XBF.Types
{
    public class XbfStringTable : XbfTable<string>
    {
        public XbfStringTable(BinaryReader reader, Version fileVersion) : base(reader, fileVersion)
        {
        }

        protected override string ReadValue(BinaryReader reader, Version fileVersion)
        {
            uint sizeOfVal = reader.ReadUInt32();
            char[] val = reader.ReadChars((int)sizeOfVal);

            if (fileVersion.Major == 2)
            {
                // XBFv2 pads strings out with \0 in 16-bit chars for some reason.
                reader.ReadChar();
            }

            return new string(val);
        }
    }
}
