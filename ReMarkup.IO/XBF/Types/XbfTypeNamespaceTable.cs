﻿namespace ReMarkup.IO.XBF.Types
{
    public class XbfTypeNamespaceTable : XbfTable<TypeNamespaceEntry>
    {
        public XbfTypeNamespaceTable(BinaryReader reader, Version fileVersion) : base(reader, fileVersion)
        {
        }

        protected override TypeNamespaceEntry ReadValue(BinaryReader reader, Version fileVersion)
        {
            var assemblyId = reader.ReadUInt32();
            var stringId = reader.ReadUInt32();
            return new TypeNamespaceEntry(assemblyId, stringId);
        }
    }

    public readonly record struct TypeNamespaceEntry(uint AssemblyId, uint StringId);
}
