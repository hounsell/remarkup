﻿namespace ReMarkup.IO.XBF.Types
{
    public class XbfTypeTable : XbfTable<TypeEntry>
    {
        public XbfTypeTable(BinaryReader reader, Version fileVersion) : base(reader, fileVersion)
        {
        }

        protected override TypeEntry ReadValue(BinaryReader reader, Version fileVersion)
        {
            var flags = (TypeFlags)reader.ReadUInt32();
            var namespaceId = reader.ReadUInt32();
            var stringId = reader.ReadUInt32();

            return new TypeEntry(flags, namespaceId, stringId);
        }
    }

    public readonly record struct TypeEntry(TypeFlags Flags, uint NamespaceId, uint StringId);

    [Flags]
    public enum TypeFlags
    {
        None = 0,
        IsMarkupDirective = 1
    }
}
