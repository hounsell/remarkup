﻿namespace ReMarkup.IO.XBF.Types
{
    public class XbfUInt32Table : XbfTable<uint>
    {
        public XbfUInt32Table(BinaryReader reader, Version fileVersion) : base(reader, fileVersion)
        {
        }

        protected override uint ReadValue(BinaryReader br, Version fv)
        {
            return br.ReadUInt32();
        }
    }
}
