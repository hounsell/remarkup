﻿using ReMarkup.IO.XBF.Nodes;
using ReMarkup.IO.XBF.Types;
using System.Collections.Immutable;
using System.Text;

namespace ReMarkup.IO.XBF
{
    public class XbfFile
    {
        public Version FileVersion { get; }

        public XbfHeader Header { get; }

        public XbfTable<string> StringTable { get; }

        public XbfTable<AssemblyEntry> AssemblyTable { get; }

        public XbfTable<TypeNamespaceEntry> TypeNamespaceTable { get; }

        public XbfTable<TypeEntry> TypeTable { get; }

        public XbfTable<PropertyEntry> PropertyTable { get; }

        public XbfTable<uint> XmlNamespaceTable { get; }

        public XbfObject RootNode { get; }

        public ImmutableArray<XbfNamespace> Namespaces { get; }

        protected XbfFile(
            XbfHeader header,
            XbfTable<string> strings,
            XbfTable<AssemblyEntry> assemblies,
            XbfTable<TypeNamespaceEntry> typeNamespaces,
            XbfTable<TypeEntry> types,
            XbfTable<PropertyEntry> properties,
            XbfTable<uint> xmlNamespaces,
            XbfObject rootNode,
            IEnumerable<XbfNamespace> namespaces
        )
        {
            Header = header;
            FileVersion = new Version((int)header.MajorFileVersion, (int)header.MinorFileVersion);
            StringTable = strings;
            AssemblyTable = assemblies;
            TypeNamespaceTable = typeNamespaces;
            TypeTable = types;
            PropertyTable = properties;
            XmlNamespaceTable = xmlNamespaces;
            RootNode = rootNode;
            Namespaces = namespaces.ToImmutableArray();
        }

        public static XbfFile Load(string? path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new FileNotFoundException();
            }

            using FileStream fStr = new(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            using BinaryReader br = new(fStr, Encoding.Unicode);

            var header = new XbfHeader(br);
            if (!header.IsMagicValid)
            {
                throw new InvalidFormatException();
            }

            var version = new Version((int)header.MajorFileVersion, (int)header.MinorFileVersion);

            var strings = new XbfStringTable(br, version);
            var assemblies = new XbfAssemblyTable(br, version);
            var typeNamespaces = new XbfTypeNamespaceTable(br, version);
            var types = new XbfTypeTable(br, version);
            var properties = new XbfPropertyTable(br, version);
            var xmlNamespaces = new XbfUInt32Table(br, version);

            var (rootNode, namespaces) = ReadNodes(br);
            if (rootNode is null)
            {
                throw new InvalidFormatException();
            }

            return new XbfFile(header, strings, assemblies, typeNamespaces, types, properties, xmlNamespaces, rootNode, namespaces);
        }

        private static (XbfObject?, IEnumerable<XbfNamespace>) ReadNodes(BinaryReader br)
        {
            XbfObject? rootNode = null;
            List<XbfNamespace> namespaces = new();
            Stack<XbfObject> objectStack = new();
            Stack<XbfProperty> propertyStack = new();

            XbfLineInfo currentLineInfo = new()
            {
                LineNumber = 0,
                LinePosition = 0
            };

            // check if there's still more to read
            while (br.BaseStream.Length != br.BaseStream.Position)
            {
                XbfNodeType type = (XbfNodeType)br.ReadByte();

                switch (type)
                {
                    case XbfNodeType.StartObject:
                        {
                            XbfObject xo = new(br, currentLineInfo);

                            // Is this the first Object?
                            // Then it's the root node.
                            rootNode ??= xo;

                            // Push it to the object stack
                            objectStack.Push(xo);
                            continue;
                        }
                    case XbfNodeType.EndObject:
                        {
                            // are we within a property?
                            if (propertyStack.Count > 0)
                            {
                                // set the current object as the property's value
                                propertyStack.Peek().AddValue(objectStack.Peek());
                            }

                            // Current object is finished with, pop it from the stack
                            objectStack.Pop();
                            continue;
                        }
                    case XbfNodeType.StartProperty:
                        {
                            XbfProperty xp = new(br, currentLineInfo);

                            // Push it to the property stack
                            propertyStack.Push(xp);
                            continue;
                        }
                    case XbfNodeType.EndProperty:
                        {
                            // are we within an object? To be honest, I think this should probably always be true for a valid XBF file, but check all the same.
                            if (objectStack.Count > 0)
                            {
                                // add the property to the current object
                                objectStack.Peek().AddProperty(propertyStack.Peek());
                            }

                            // Current property is complete, pop it from the stack
                            propertyStack.Pop();
                            continue;
                        }
                    case XbfNodeType.Text:
                        {
                            XbfText xt = new(br, currentLineInfo);

                            // are we currently inside a property? should probably always be true I think
                            if (propertyStack.Count > 0)
                            {
                                // add it as a value for the current property
                                propertyStack.Peek().AddValue(xt);
                            }
                            continue;
                        }
                    case XbfNodeType.Value:
                        {
                            XbfValue xv = new(br, currentLineInfo);

                            // are we currently inside a property? should probably always be true I think
                            if (propertyStack.Count > 0)
                            {
                                // add it as a value for the current property
                                propertyStack.Peek().AddValue(xv);
                            }
                            continue;
                        }
                    case XbfNodeType.Namespace:
                        {
                            XbfNamespace xn = new(br, currentLineInfo);

                            // These don't get added in the normal hierarchy.
                            namespaces.Add(xn);
                            continue;
                        }
                    case XbfNodeType.LineInfo:
                        {
                            XbfLineInfo newLineInfo = new()
                            {
                                LineNumber = (uint)(currentLineInfo.LineNumber + br.ReadInt16()),
                                LinePosition = (uint)(currentLineInfo.LinePosition + br.ReadInt16())
                            };

                            currentLineInfo = newLineInfo;
                            continue;
                        }
                    case XbfNodeType.LineInfoAbsolute:
                        {
                            currentLineInfo = new XbfLineInfo()
                            {
                                LineNumber = br.ReadUInt32(),
                                LinePosition = br.ReadUInt32()
                            };
                            continue;
                        }
                    case XbfNodeType.EndOfAttributes:
                    case XbfNodeType.EndOfStream:
                    default:
                        continue;
                }
            }

            return (rootNode, namespaces);
        }
    }
}
