﻿using System.Collections.Immutable;

namespace ReMarkup.IO.XBF
{
    public readonly struct XbfHeader
    {
        private static readonly ImmutableArray<byte> _fileMagic = (new byte[] { 0x58, 0x42, 0x46, 0x00 }).ToImmutableArray();

        public ImmutableArray<byte> MagicNumber { get; }

        public bool IsMagicValid => MagicNumber.SequenceEqual(_fileMagic);

        public uint MetadataSize { get; }

        public uint NodeSize { get; }

        public uint MajorFileVersion { get; }

        public uint MinorFileVersion { get; }

        public ulong StringTableOffset { get; }

        public ulong AssemblyTableOffset { get; }

        public ulong TypeNamespaceTableOffset { get; }

        public ulong TypeTableOffset { get; }

        public ulong PropertyTableOffset { get; }

        public ulong XmlNamespaceTableOffset { get; }

        public ImmutableArray<char> Hash { get; }

        public XbfHeader(BinaryReader br)
        {
            MagicNumber = br.ReadBytes(4).ToImmutableArray();
            MetadataSize = br.ReadUInt32();
            NodeSize = br.ReadUInt32();
            MajorFileVersion = br.ReadUInt32();
            MinorFileVersion = br.ReadUInt32();
            StringTableOffset = br.ReadUInt64();
            AssemblyTableOffset = br.ReadUInt64();
            TypeNamespaceTableOffset = br.ReadUInt64();
            TypeTableOffset = br.ReadUInt64();
            PropertyTableOffset = br.ReadUInt64();
            XmlNamespaceTableOffset = br.ReadUInt64();
            Hash = br.ReadChars(0x20).ToImmutableArray();
        }
    }
}
