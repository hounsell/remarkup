﻿using System.Collections.Immutable;

namespace ReMarkup.IO.XBF
{
    public abstract class XbfTable<T>
    {
        public uint Size { get; }

        public IReadOnlyList<T> Values { get; }

        protected abstract T ReadValue(BinaryReader reader, Version fileVersion);

        public XbfTable(BinaryReader reader, Version fileVersion)
        {
            Size = reader.ReadUInt32();

            List<T> _values = new((int)Size);

            for (int i = 0; i < Size; i++)
            {
                _values.Insert(i, ReadValue(reader, fileVersion));
            }

            Values = _values.ToImmutableArray();
        }
    }
}
