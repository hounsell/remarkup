﻿using AvaloniaEdit.Document;

namespace ReMarkup.UI.ViewModels;

public class MainViewModel : ViewModelBase
{
    public TextDocument XmlOutput { get; set; } = new TextDocument("<xml></xml>");
}
