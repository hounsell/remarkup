﻿using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Threading;
using AvaloniaEdit.Document;
using AvaloniaEdit.TextMate;
using ReMarkup.IO.XBF;
using ReMarkup.IO.XBF.Output;
using ReMarkup.UI.ViewModels;
using System.ComponentModel;
using System.Threading.Tasks;
using TextMateSharp.Grammars;

namespace ReMarkup.UI.Views;

public partial class MainView : UserControl
{
    private RegistryOptions _tmOptions;
    private TextMate.Installation _tmInstall;

    public MainView()
    {
        InitializeComponent();
        _tmOptions = new RegistryOptions(ThemeName.DarkPlus);
        _tmInstall = XmlEditor.InstallTextMate(_tmOptions);
        _tmInstall.SetGrammar(_tmOptions.GetScopeByExtension(".xml"));
    }

    private async void OpenButton_OnClick(object? sender, RoutedEventArgs e)
    {
        var result = await OpenFileAsync((Window)Parent!);
        if (result != null)
        {
            DataContext = result;
        }
    }

    private async Task<MainViewModel?> OpenFileAsync(Window parent)
    {
        OpenFileDialog ofd = new()
        {
            AllowMultiple = false
        };

        string[]? result = await ofd.ShowAsync(parent);
        if (result is not { Length: 1 })
        {
            return null;
        }

        string path = result[0];

        var file = XbfFile.Load(path);
        XamlOutput xo = new();
        var xml = xo.GetOutput(file);

        var vm = new MainViewModel
        {
            XmlOutput = new TextDocument(xml.ToString())
        };

        return vm;
    }
}
